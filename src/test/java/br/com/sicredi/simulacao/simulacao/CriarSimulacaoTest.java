package br.com.sicredi.simulacao.simulacao;

import br.com.sicredi.simulacao.model.SimulacaoDTO;
import io.restassured.http.ContentType;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import com.google.gson.Gson;

import java.math.BigDecimal;

import static io.restassured.RestAssured.*;

public class CriarSimulacaoTest {
    @Before
    public void config(){
        baseURI= "http://localhost";
        port = 8080;
        basePath = "/api/v1";
    }

    @Test
    public void deveRetornarCreated(){
        SimulacaoDTO dto = SimulacaoDTO.builder()
                .nome("everton")
                .cpf("99999999900")
                .email("everton.nunes@hotmail.com")
                .valor(new BigDecimal(30000))
                .parcelas(2)
                .seguro(true)
                .build();

        given().
                body(new Gson().toJson(dto))
                .contentType(ContentType.JSON)
                .when().
                    post("/simulacoes")
                .then()
                    .log().all()
                .assertThat().statusCode(HttpStatus.CREATED.value());
    }

    @Test
    public void deveRetornarBadRequest(){
        SimulacaoDTO dto = SimulacaoDTO.builder()
                .nome("everton")
                .email("everton.nunes@hotmail.com")
                .valor(new BigDecimal(30000))
                .parcelas(2)
                .seguro(true)
                .build();

        given().
                body(new Gson().toJson(dto))
                .contentType(ContentType.JSON)
                .when().
                post("/simulacoes")
                .then()
                .log().all()
                .assertThat().statusCode(HttpStatus.BAD_REQUEST.value());
    }

    @Test
    public void deveRetornarConflit(){
        SimulacaoDTO dto = SimulacaoDTO.builder()
                .nome("everton")
                .cpf("99999999900")
                .email("everton.nunes@hotmail.com")
                .valor(new BigDecimal(30000))
                .parcelas(2)
                .seguro(true)
                .build();
        given().
                body(new Gson().toJson(dto))
                .contentType(ContentType.JSON)
                .when().
                post("/simulacoes")
                .then()
                .log().all()
                .assertThat().statusCode(HttpStatus.CONFLICT.value());
    }

}
