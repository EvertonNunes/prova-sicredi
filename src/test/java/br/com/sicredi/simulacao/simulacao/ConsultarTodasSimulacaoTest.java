package br.com.sicredi.simulacao.simulacao;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.not;

public class ConsultarTodasSimulacaoTest {

    @Before
    public void config() {
        baseURI = "http://localhost";
        port = 8080;
        basePath = "/api/v1";
    }

    @Test
    public void deveRetornarSuccess() {
        given()
                .when()
                .get("/simulacoes")
                .then().body("findAll", not(empty()));
    }

    @Test
    public void deveRetornarNoContent() {
        given()
                .when()
                .get("/simulacoes")
                .then().body("findAll", empty()).assertThat().statusCode(HttpStatus.NO_CONTENT.value());
    }
}
