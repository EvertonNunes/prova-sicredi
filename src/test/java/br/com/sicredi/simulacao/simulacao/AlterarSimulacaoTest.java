package br.com.sicredi.simulacao.simulacao;

import br.com.sicredi.simulacao.model.SimulacaoDTO;
import com.google.gson.Gson;
import io.restassured.http.ContentType;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;

import java.math.BigDecimal;

import static io.restassured.RestAssured.*;

public class AlterarSimulacaoTest {
    @Before
    public void config(){
        baseURI= "http://localhost";
        port = 8080;
        basePath = "/api/v1";
    }
    public void criarSimulacao(){
        SimulacaoDTO dto = SimulacaoDTO.builder()
                .nome("everton")
                .cpf("99999999922")
                .email("everton.nunes@hotmail.com")
                .valor(new BigDecimal(30000))
                .parcelas(2)
                .seguro(true)
                .build();

        given().
                body(new Gson().toJson(dto))
                .contentType(ContentType.JSON)
        .when()
        .post("/simulacoes");
    }

    @Test
    public void deveRetornarOk(){
        criarSimulacao();
        SimulacaoDTO dto = SimulacaoDTO.builder()
                .nome("evertonTESTE")
                .cpf("99999999922")
                .email("everton.nunes@hotmail.com")
                .valor(new BigDecimal(30000))
                .parcelas(2)
                .seguro(true)
                .build();

        given().
                body(new Gson().toJson(dto))
                .contentType(ContentType.JSON)
                .when().
                put("/simulacoes/99999999922")
                .then()
                .log().all()
                .assertThat().statusCode(HttpStatus.OK.value());
    }

    @Test
    public void deveRetornarNotFound(){
        SimulacaoDTO dto = SimulacaoDTO.builder()
                .nome("evertonTESTE")
                .cpf("00000000000")
                .email("everton.nunes@hotmail.com")
                .valor(new BigDecimal(30000))
                .parcelas(2)
                .seguro(true)
                .build();

        given().
                body(new Gson().toJson(dto))
                .contentType(ContentType.JSON)
                .when().
                put("/simulacoes/00000000000")
                .then()
                .log().all()
                .assertThat().statusCode(HttpStatus.NOT_FOUND.value());
    }
}


