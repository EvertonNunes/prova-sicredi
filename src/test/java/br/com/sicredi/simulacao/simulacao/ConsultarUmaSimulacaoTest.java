package br.com.sicredi.simulacao.simulacao;

import br.com.sicredi.simulacao.model.SimulacaoDTO;
import com.google.gson.Gson;
import io.restassured.http.ContentType;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;

import java.math.BigDecimal;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.not;

public class ConsultarUmaSimulacaoTest {
    @Before
    public void config(){
        baseURI= "http://localhost";
        port = 8080;
        basePath = "/api/v1";
    }
    public void criarSimulacao(){
        SimulacaoDTO dto = SimulacaoDTO.builder()
                .nome("everton")
                .cpf("99999991100")
                .email("everton.nunes@hotmail.com")
                .valor(new BigDecimal(30000))
                .parcelas(2)
                .seguro(true)
                .build();

        given().
                body(new Gson().toJson(dto))
                .contentType(ContentType.JSON)
                .when()
                .post("/simulacoes");
    }

    @Test
    public void deveRetornarSucess() {
        criarSimulacao();
        given()
                .when()
                .get("/simulacoes/99999991100").
                then().body("findAll", not(empty()));
    }
    @Test
    public void deveRetornarNotFound(){

        given()
                .when()
                .get("/simulacoes/11111111111")
                .then().assertThat().statusCode(HttpStatus.NOT_FOUND.value());

    }

}
