package br.com.sicredi.simulacao.simulacao;

import br.com.sicredi.simulacao.model.SimulacaoDTO;
import com.google.gson.Gson;
import io.restassured.http.ContentType;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;

import java.math.BigDecimal;

import static io.restassured.RestAssured.*;

public class RemoverSimulacaoTest {
    @Before
    public void init() {
        baseURI = "http://localhost";
        port = 8080;
        basePath = "/api/v1";

    }

    public Integer criarSimulacao() {
        SimulacaoDTO dto = SimulacaoDTO.builder()
                .nome("everton")
                .cpf("99999999922")
                .email("everton.nunes@hotmail.com")
                .valor(new BigDecimal(30000))
                .parcelas(2)
                .seguro(true)
                .build();

        return given().
                body(new Gson().toJson(dto))
                .contentType(ContentType.JSON)
                .when()
                .post("/simulacoes").then().extract().path("id");
    }

    @Test
    public void deveRetornarNoContent() {
        Integer id = criarSimulacao();
        given()
                .when()
                .delete("/simulacoes/" + id)
                .then().assertThat()
                .statusCode(HttpStatus.NO_CONTENT.value());

    }

    @Test
    public void deveRetornarNotFound() {
        given()
                .when()
                .delete("/simulacoes/123456")
                .then().assertThat()
                .statusCode(HttpStatus.NOT_FOUND.value());
    }


}
