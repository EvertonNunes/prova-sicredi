package br.com.sicredi.simulacao.restricao;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;

import static io.restassured.RestAssured.*;

public class ConsultarRestricaoTest {
    @Before
    public void config(){
        baseURI= "http://localhost";
        port = 8080;
        basePath = "/api/v1";
    }
    @Test
    public void deveRetornarNoContent(){
        given().when().get("/restricoes/970932364014").then().assertThat().statusCode(HttpStatus.NO_CONTENT.value());

    }

    @Test
    public void deveRetornarRestricao(){
        given().when();get("/restricoes/60094146012").then().assertThat().statusCode(HttpStatus.OK.value());
    }


}
