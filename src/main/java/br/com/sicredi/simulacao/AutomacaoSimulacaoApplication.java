package br.com.sicredi.simulacao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AutomacaoSimulacaoApplication {

    public static void main(String[] args) {
        SpringApplication.run(AutomacaoSimulacaoApplication.class, args);
    }

}
